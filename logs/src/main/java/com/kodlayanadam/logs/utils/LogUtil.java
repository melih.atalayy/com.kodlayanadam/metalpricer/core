package com.kodlayanadam.logs.utils;

public class LogUtil {
    public static final String USERNAME = "userName";
    public static final String CLIENT_IP = "clientIp";
    public static final String CLIENT_IP2 = "clientIp2";
    public static final String HOST_IP = "hostIp";
    public static final String METHOD = "method";
    public static final String PATH = "path";
    public static final String HEADERS = "headers";
    public static final String PARAMETERS = "parameters";
    public static final String BODY = "body";
    public static final String LOG_TRACK_ID_MDC_KEY = "logTrackId";
}
