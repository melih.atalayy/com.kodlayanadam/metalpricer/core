package com.kodlayanadam.logs.mdc;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kodlayanadam.logs.utils.LogUtil;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.MDC;
import org.slf4j.Logger;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.Principal;
import java.util.*;

public class MdcHandler {
    ObjectMapper objectMapper;

    public MdcHandler(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    private  void MdcPutHandle(String key, String val){
        if(!key.isEmpty() && !val.isEmpty()){
            MDC.put(key," || "+key+"="+val);
        }
    }

    public void MdcPut(HttpServletRequest request, HttpServletResponse response, Object body, Logger logger) throws UnknownHostException, JsonProcessingException {
        StringBuilder reMessage = new StringBuilder();
        Map<String,String> parameters = getParameters(request);
        String hostIp = InetAddress.getLocalHost().getHostAddress();
        String clientIp = getClientIPFromRequest(request);
        String clientIp2 = getIPFromRequest(request);
        String userName = getUserName(request);
        String method = request.getMethod();
        String path = request.getRequestURI();
        String headers;

        try {
            MdcPutHandle(LogUtil.LOG_TRACK_ID_MDC_KEY,generateLogTrackId());
            MdcPutHandle(LogUtil.USERNAME,userName);
            MdcPutHandle(LogUtil.CLIENT_IP,clientIp);
            MdcPutHandle(LogUtil.CLIENT_IP2,clientIp2);
            MdcPutHandle(LogUtil.HOST_IP,hostIp);
            MdcPutHandle(LogUtil.METHOD,method);
            MdcPutHandle(LogUtil.PATH,path);

            if(response == null){
                headers=objectMapper.writeValueAsString(getHeadersRequest(request));
                MdcPutHandle(LogUtil.HEADERS,headers);
                reMessage.append("REQUEST = {");
                if(!parameters.isEmpty()){
                    MdcPutHandle(LogUtil.PARAMETERS,objectMapper.writeValueAsString(parameters));
                    reMessage.append(" parameters = [").append(parameters).append("] ");
                }
                if(!Objects.isNull(body)){
                    MdcPutHandle(LogUtil.BODY,objectMapper.writeValueAsString(body));
                    reMessage.append(" body = [").append(body).append("]");
                }
            }
            else{
                headers=objectMapper.writeValueAsString(getHeadersResponse(response));
                MDC.put(LogUtil.HEADERS,headers);
                reMessage.append("RESPONSE = {");
                reMessage.append(" status = [").append(response.getStatus()).append("]");
                reMessage.append(" responseBody = [").append(body).append("]");
            }
            reMessage.append("}");
            logger.info("Log : {}",reMessage);

        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            MDC.clear();
            reMessage.setLength(0);
        }
    }

    private Map<String,String> getHeadersRequest(HttpServletRequest request) {
        Map<String,String> headers = new HashMap<>();
        Collection<String> headerMap = Collections.list(request.getHeaderNames());
        for(var str : headerMap) {
            headers.put(str,request.getHeader(str));
        }
        return headers;
    }

    private Map<String,String> getHeadersResponse(HttpServletResponse response) {
        Map<String,String> headers = new HashMap<>();
        Collection<String> headerMap = response.getHeaderNames();
        for(String str : headerMap) {
            headers.put(str,response.getHeader(str));
        }
        return headers;
    }

    private Map<String,String> getParameters(HttpServletRequest request) {
        Map<String,String> parameters = new HashMap<>();
        Enumeration<String> params = request.getParameterNames();
        while(params.hasMoreElements()) {
            String paramName = params.nextElement();
            String paramValue = request.getParameter(paramName);
            parameters.put(paramName,paramValue);
        }
        return parameters;
    }

    private String getUserName(HttpServletRequest request){
        Principal principal = request.getUserPrincipal();
        if(principal != null){
            return principal.getName();
        }
        return "";
    }

    private String getClientIPFromRequest(HttpServletRequest request) {
        String clientIp = request.getHeader("clientIp");
        if (clientIp == null || "".equals(clientIp)) {
            clientIp = "";
        }

        return clientIp;
    }

    private String getIPFromRequest(HttpServletRequest request) {
        String userIP = request.getHeader("client-ip");
        if (userIP == null || "".equals(userIP)) {
            userIP = request.getRemoteAddr();
        }

        return userIP;
    }

    private String generateLogTrackId() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }
}
