package com.kodlayanadam.logs.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.net.UnknownHostException;

public interface LoggingService {
    void displayRequest(HttpServletRequest request, Object body) throws UnknownHostException, JsonProcessingException;

    void displayResponse(HttpServletRequest request, HttpServletResponse response, Object body) throws UnknownHostException, JsonProcessingException;

}
