package com.kodlayanadam.logs.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kodlayanadam.logs.mdc.MdcHandler;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.net.UnknownHostException;

@Service
public class LoggingServiceImpl implements LoggingService{

    Logger logger = LoggerFactory.getLogger("LoggingServiceImpl");
    ObjectMapper objectMapper;

    public LoggingServiceImpl(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    public void displayRequest(HttpServletRequest request, Object body) throws UnknownHostException, JsonProcessingException {
        MdcHandler mdcHandler = new MdcHandler(objectMapper);
        mdcHandler.MdcPut(request,null,body,logger);
    }

    @Override
    public void displayResponse(HttpServletRequest request, HttpServletResponse response, Object body) throws UnknownHostException, JsonProcessingException {
        MdcHandler mdcHandler = new MdcHandler(objectMapper);
        mdcHandler.MdcPut(request,response,body,logger);
    }

}
