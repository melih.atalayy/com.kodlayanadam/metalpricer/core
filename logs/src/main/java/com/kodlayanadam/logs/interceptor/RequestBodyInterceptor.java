package com.kodlayanadam.logs.interceptor;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.kodlayanadam.logs.service.LoggingService;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.RequestBodyAdviceAdapter;


import java.lang.reflect.Type;
import java.net.UnknownHostException;

@ControllerAdvice
public class RequestBodyInterceptor extends RequestBodyAdviceAdapter {

    LoggingService logService;
    HttpServletRequest request;

    public RequestBodyInterceptor(LoggingService logService, HttpServletRequest request) {
        this.logService = logService;
        this.request = request;
    }

    @Override
    public Object afterBodyRead(Object body, HttpInputMessage inputMessage, MethodParameter parameter, Type targetType, Class<? extends HttpMessageConverter<?>> converterType) {
        try {
            logService.displayRequest(request,body);
        } catch (UnknownHostException e) {
            throw new RuntimeException(e);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        return super.afterBodyRead(body, inputMessage, parameter, targetType, converterType);
    }


    @Override
    public boolean supports(MethodParameter methodParameter, Type targetType, Class<? extends HttpMessageConverter<?>> converterType) {
        return true;
    }
}
