package com.kodlayanadam.common.dto;

import com.kodlayanadam.common.enums.ServiceEnum;

import java.util.Objects;

public class ServiceResponse<T> extends BaseDto{

    private boolean resultOk;
    private String resultMessage;
    private Integer resultCode;
    private Object error;
    private T output;
    public ServiceResponse() {
        super();
    }
    public ServiceResponse(boolean resultOk, String resultMessage, Integer resultCode, T output) {
        super();
        this.resultOk = resultOk;
        this.resultMessage = resultMessage;
        this.resultCode = resultCode;
        this.output = output;
    }
    public ServiceResponse(ServiceEnum serviceEnum, T output) {
        super();
        this.resultOk = serviceEnum.isResult();
        this.resultMessage = serviceEnum.getResultMessage();
        this.resultCode = serviceEnum.getResultCode();
        this.output = output;
    }

    public ServiceResponse(ServiceEnum serviceEnum, Object error, T output) {
        super();
        this.resultOk = serviceEnum.isResult();
        this.resultMessage = serviceEnum.getResultMessage();
        this.resultCode = serviceEnum.getResultCode();
        this.error=error;
        this.output = output;
    }

    public ServiceResponse(ServiceEnum serviceEnum) {
        super();
        this.resultOk = serviceEnum.isResult();
        this.resultMessage = serviceEnum.getResultMessage();
        this.resultCode = serviceEnum.getResultCode();
    }

    public boolean isResultOk() {
        return resultOk;
    }

    public void setResultOk(boolean resultOk) {
        this.resultOk = resultOk;
    }

    public String getResultMessage() {
        return resultMessage;
    }

    public void setResultMessage(String resultMessage) {
        this.resultMessage = resultMessage;
    }

    public Integer getResultCode() {
        return resultCode;
    }

    public void setResultCode(Integer resultCode) {
        this.resultCode = resultCode;
    }

    public Object getError() {
        return error;
    }

    public void setError(Object error) {
        this.error = error;
    }

    public T getOutput() {
        return output;
    }

    public void setOutput(T output) {
        this.output = output;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ServiceResponse<?> that = (ServiceResponse<?>) o;
        return resultOk == that.resultOk && Objects.equals(resultMessage, that.resultMessage) && Objects.equals(resultCode, that.resultCode) && Objects.equals(error, that.error) && Objects.equals(output, that.output);
    }

    @Override
    public int hashCode() {
        return Objects.hash(resultOk, resultMessage, resultCode, error, output);
    }
}
