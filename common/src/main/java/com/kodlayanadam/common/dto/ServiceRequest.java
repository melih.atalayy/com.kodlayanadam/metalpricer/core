package com.kodlayanadam.common.dto;

import java.util.Objects;

public class ServiceRequest<T> extends BaseDto{
    private T input;

    public ServiceRequest() {
        super();
    }

    public ServiceRequest(T input) {
        super();
        this.input = input;
    }

    public T getInput() {
        return input;
    }

    public void setInput(T input) {
        this.input = input;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ServiceRequest<?> that = (ServiceRequest<?>) o;
        return Objects.equals(input, that.input);
    }

    @Override
    public int hashCode() {
        return Objects.hash(input);
    }
}
