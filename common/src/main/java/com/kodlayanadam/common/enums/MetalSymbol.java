package com.kodlayanadam.common.enums;

public enum MetalSymbol {
    XAU, //Gold
    XAG, //Silver
    XPT, //Platinum
    XPD //Palladium
}
