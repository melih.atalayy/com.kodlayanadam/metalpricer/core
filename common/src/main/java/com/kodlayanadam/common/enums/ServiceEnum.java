package com.kodlayanadam.common.enums;

public enum ServiceEnum {

    OK(true,"SUCCESS",0),
    NOT_OK(false,"FAIL",-1);

    private final boolean result;
    private final String resultMessage;
    private final Integer resultCode;

    ServiceEnum(boolean result, String resultMessage, Integer resultCode) {
        this.result = result;
        this.resultMessage = resultMessage;
        this.resultCode = resultCode;
    }

    public boolean isResult() {
        return result;
    }

    public String getResultMessage() {
        return resultMessage;
    }

    public Integer getResultCode() {
        return resultCode;
    }
}
