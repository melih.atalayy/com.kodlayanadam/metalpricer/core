package com.kodlayanadam.common.enums;

public enum CurrencyCode {
    TRY,
    USD,
    EUR,
    GBP
}
