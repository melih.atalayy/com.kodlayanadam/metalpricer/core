package com.kodlayanadam.exception.handler;

import com.kodlayanadam.exception.ServiceException;
import com.kodlayanadam.common.dto.ServiceResponse;
import com.kodlayanadam.exception.error.ApiError;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class ServiceExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {ServiceException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public <T> ResponseEntity<ServiceResponse<T>> handleServiceException(ServiceException exception){
        ApiError apiError = new ApiError(HttpStatus.NOT_FOUND);
        apiError.setMessage(exception.getMessage());
        ServiceResponse<T> serviceReturnObject = new ServiceResponse<>(exception.getServiceEnum());
        return buildResponseEntity(apiError,serviceReturnObject);
    }


    private <T> ResponseEntity<ServiceResponse<T>> buildResponseEntity(ApiError apiError, ServiceResponse<T> serviceReturnObject) {
        serviceReturnObject.setError(apiError);
        return new ResponseEntity<>(serviceReturnObject, apiError.getStatus());
    }

}
