package com.kodlayanadam.mapping;

import com.kodlayanadam.common.dto.ServiceResponse;
import com.kodlayanadam.common.enums.ServiceEnum;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ServiceMapper {

    default  <T> ServiceResponse<T> mapToSuccessResponse(T t){
        return new ServiceResponse<>(ServiceEnum.OK,t);
    }
    default  <T> ServiceResponse<T> mapToFailResponse(T t){
        return new ServiceResponse<>(ServiceEnum.NOT_OK,t);
    }
    default  <T> ServiceResponse<T> mapToResponse(T t,ServiceEnum serviceEnum){
        return new ServiceResponse<>(serviceEnum,t);
    }
}
